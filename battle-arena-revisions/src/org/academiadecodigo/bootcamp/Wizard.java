package org.academiadecodigo.bootcamp;

public class Wizard extends Character {

    public Wizard(){
        super(100,5);
    }

    @Override
    public void spellCast(Character enemy) {
        setDamage(20);
        enemy.sufferDamage(getDamage());
    }
}
