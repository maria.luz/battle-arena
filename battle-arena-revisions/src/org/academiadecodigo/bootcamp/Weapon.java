package org.academiadecodigo.bootcamp;

public abstract class Weapon {

    private int damage = 20;

    public void weaponAttack(){

    }

    @Override
    public String toString() {
        return "Weapon{" +
                "damage=" + damage +
                '}';
    }
}
