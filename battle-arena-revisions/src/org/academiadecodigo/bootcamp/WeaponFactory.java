package org.academiadecodigo.bootcamp;

public class WeaponFactory {

    public  WeaponFactory() {
        produceWeapon();
    }



    public static Weapon produceWeapon() {
       int produce = (int) Math.ceil(Math.random() * 3);
       if (produce < 2) {
           return new Sword();
       }
       if (produce == 2) {
           return new Hammer();
       }
       else {
           return new Wand();
       }
    }


}
