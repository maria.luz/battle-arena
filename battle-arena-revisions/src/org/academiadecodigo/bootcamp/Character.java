package org.academiadecodigo.bootcamp;

public abstract class Character {
    private int health;
    private boolean dead;
    private int damage;
    private Weapon weapon;



    public Character(int health, int damage){
        this.health= health;
        this.damage= damage;
        this.dead=false;
        this.weapon= WeaponFactory.produceWeapon();

    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDamage() {
        return damage;
    }

    public boolean isDead() {
        return dead;
    }

    public void spellCast(Character enemy){
        enemy.sufferDamage(damage);
    }

    public  void  attack(Character enemy){
        int criticalAttack = (int) Math.random() * 10;
        if(criticalAttack > 7){
            damage = damage * 2;
            System.out.println("It's a critical hit. You attacked with  " + weapon + " and you took " + damage + " damage!" );

        }
        enemy.sufferDamage(damage);
        System.out.println("You attacked with " + weapon + " and you took " + damage + " damage!");

    }

    public void characterDie(){
        if (health <= 0){
            dead = true;
        }
    }

    public void sufferDamage(int damage){
        this.health -= damage;
    }

    @Override
    public String toString() {
        return "Character{" +
                "health=" + health +
                ", dead=" + dead +
                ", damage=" + damage +
                ", weapon=" + weapon +
                '}';
    }
}
