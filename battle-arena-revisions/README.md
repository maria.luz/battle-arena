# battle-arena

week 6 revisions exercise

## Description

- Text-based game (think "Sniper Elite"), where one character attacks another until they're dead.
- We are going to need different Characters/Heroes (think RPG-style: a Troll, a Gladiator, an EvilSorceress, a Wizard, etc.)
- The characters can attack by using a Weapon (we're going to need different weapons, and some kind of weapon factory), or cast a spell.
- There should be normal attacks and a crit attacks (but fewer crit attacks than normal ones).
- A Hero can suffer damage and die.

## Additional

- Ways to incorporate nested classes: a WeaponType enum that only serves the WeaponFactory.
- Ways to incorporate collections: a Hero can have a collection of weapons or a book of spells.
- Ways to incorporate streams: the spells can be read from an external file into the book!